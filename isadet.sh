#!/bin/sh
set -e

PKG="$1"
if [ -z "$PKG" ]; then echo >&2 "Usage: $0 <package.deb>"; exit 1; fi
DIR=`mktemp -d`;
if [ -z "$DIR" ]; then echo >&2 "mktemp failed"; exit 1; fi
trap "rm -rf '$DIR'" EXIT
dpkg -x "$PKG" "$DIR"
cd "$DIR"
EXT=$(find -type f -print0|xargs -0 ~/c/antisse/ext)
if [ -n "$EXT" ]; then echo "$EXT $PKG"; fi
