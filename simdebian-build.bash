#!/bin/bash
DEB_HOST_ARCH=$(dpkg-architecture -qDEB_HOST_ARCH)
for I in *.dsc; do
	if [[ -r ${I%.dsc}_${DEB_HOST_ARCH}.changes ]]; then
		echo Skip $I : ${I%.dsc}_${DEB_HOST_ARC}.changes
	else
		sbuild -j12 -c sid-amd64 --extra-package=.. $I
	fi
done
