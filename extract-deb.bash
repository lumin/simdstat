#!/bin/bash

counter=0
debs=( $(ls *.deb) )
for I in ${debs[@]}; do
	printf "($((++counter))/${#debs[@]}) $I ..."
	dpkg -x $I .
	printf " \x1b[1;32mOK\x1b[m\n"
done
