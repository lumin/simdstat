#!/usr/bin/perl -w
use strict;

my %archs = (
    "32L Intel 80386," => "i386",
    "64L x86-64," => "amd64",
    "64L ARM aarch64," => "arm64",
    "32L ARM, EABI5" => "armhf", # no comma after
);
my %triplets = (
    "i386" => "",
    "amd64" => "",
    "armhf" => "arm-linux-gnueabihf-",
    "arm64" => "aarch64-linux-gnu-",
);

my %ins;
for (@ARGV)
{
    -f $_ or next;
    open FILE, "-|", "file", "-b", $_ or die "Can't check type of '$_'\n";
    my $type=<FILE>;
    close FILE;
    $type =~ /^ELF (32|64)-bit ([LM])SB (?:executable|shared object), (.*) version 1 \(SYSV\),/
        or next;
    my $a=$archs{"$1$2 $3"};
    open DIS, "-|", "$triplets{$a}objdump", "-d", $_ or die "Can't disasm '$_'\n";
    while (<DIS>)
    {
        next unless /^ *[0-9a-f]+:\t[0-9a-f ]+\t([a-zA-Z0-9]+)/;
        ++$ins{$1};
    }
    close DIS
}

my @sse2 = qw(orpd cvtdq2ps andpd cmpnltpd addpd psubq cmpeqsd cvtpd2dq
punpcklqdq pshuflw cvttpd2dq paddq cvttps2dq cmpneqpd cmpordpd cvtpd2ps
cmpneqsd xorpd pmuludq cmplesd subpd cvtpd2pi pshufhw cvttsd2si psrldq
movupd movmskpd cmpunordsd cmpnltsd movq2dq maskmovdqu cvtsi2sd cmppd
cvttpd2pi sqrtsd movnti cmpltsd comisd minpd ucomisd cmpnlepd minsd maxsd
mfence pshufd unpckhpd cmplepd punpckhqdq divsd mulpd cmpltpd cmpunordpd
maxpd lfence shufpd subsd movhpd mulsd cvtps2pd unpcklpd movntdq movapd
cvtsd2ss movdqu andnpd cvtps2dq movdqa cvtsd2si cmpordsd movdq2q divpd
sqrtpd cvtpi2pd movntpd pslldq cvtss2sd cmpeqpd cmpnlesd movlpd addsd
cvtdq2pd); 
my @sse3 = qw(movsldup lddqu hsubpd haddpd movshdup addsubps mwait haddps
monitor hsubps movddup addsubpd);
my @sse4_1 = qw(pmovzxbq pmovsxdq insertps pmovsxwd pblendvb ptest pmovzxwq
roundpd pmaxuw dppd phminposuw pmovzxbd dpps pminsb blendvpd extractps
packusdw pblendw pminuw pmovsxbw pmaxsb pmaxsd pmovzxbw movntdqa blendpd
blendps pminsd roundss pminud pmovsxbq pcmpeqq pmovsxwq pmovzxwd roundps
pextrb pextrd pmaxud blendvps pinsrb roundsd pmovsxbd pmulld pinsrd pmuldq
pmovzxdq mpsadbw);
my @sse4_2 = qw(pcmpestri crc32 pcmpgtq pcmpistri pcmpistrm pcmpestrm);
push @sse4_2, qw(pextrq pinsrq); # 64-bit only
#my @sse4a = qw(extrq movntsd insertq movntss);
my @ssse3 = qw(phsubd phaddd phaddw pshufb pmulhrsw psignb pabsw palignr
pabsd phsubsw phsubw phaddsw psignw pabsb pmaddubsw psignd);
my @neon = qw(vaba vabd vabs vacge vacgt vacle vaclt vadd vaddhn vand vand
vbic vbic vbif vbit vbsl vceq vcge vcgt vcle vcle vcls vclt vclt vclz vcnt
vcvt vcvt vdup veor vext vfma vfms vhadd vhsub vld vmax vmin vmla vmla vmls
vmls vmov vmov vmovl vmovn vmovun vmul vmul vmvn vneg vorn vorn vorr vorr
vpadal vpadd vpmax vpmin vqabs vqadd vqdmlal vqdmlsl vqdmul vqdmulh vqmovn
vqmovun vqneg vqrdmulh vqrshl vqrshrn vqrshrun vqshl vqshl vqshrn vqshrun
vqsub vraddhn vrecpe vrecps vrev vrhadd vrshr vrsqrte vrsqrts vrsra vrsubhn
vshl vshr vsli vsra vsri vst vsub vsubhn vswp vtbl vtbx vtrn vtst vuzp
vzip);

for (@ssse3, @sse4_1, @sse4_2)
{
    (print("sse4.2\n"),exit) if $ins{$_};
}

for (@sse3)
{
    (print("sse3\n"),exit) if $ins{$_};
}

for (@sse2)
{
    (print("sse2\n"),exit) if $ins{$_};
}

for (@neon)
{
    (print("neon\n"),exit) if $ins{$_};
}
