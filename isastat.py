#!/usr/bin/pypy3
'''
Copyright (C) 2019 Mo Zhou <lumin@debian.org>
License: MIT/Expat
requirements: pypy3
https://en.wikipedia.org/wiki/X86_instruction_listings
https://software.intel.com/sites/landingpage/IntrinsicsGuide/
https://www.nasm.us/xdoc/2.14.02/html/nasmdocb.html  <-- database file
'''
from typing import *
import argparse
import glob
import json
import magic
import os
import re
import sys

class ASMDatabase(object):
    def __init__(self, dbfile):
        pass

ISA_8086 = '''AAA AAD AAM AAS ADC ADD AND CALL CBW CLC CLD CLI CMC CMP CMPSB
CMPSW CWD DAA DAS DEC DIV ESC HLT IDIV IMUL IN INC INT INTO IRET Jcc JCXZ JMP
LAHF LDS LEA LES LOCK LODSB LODSW LOOP/LOOPx MOV MOVSB MOVSW MUL NEG NOP NOT OR
OUT POP POPF PUSH PUSHF RCL RCR REPxx RET RETN RETF ROL ROR SAHF SAL SAR SBB
SCASB SCASW SHL SHR STC STD STI STOSB STOSW SUB TEST WAIT XCHG XLAT XOR JA JAE
JB JBE JC JE JG JGE JL JLE JNA JNAE JNB JNBE JNC JNE JNG JNGE JNL JNLE JNO JNP
JNS JNZ JO JP JPE JPO JS JZ
MOVS STOS CMPS LODS SCAS
REP REPE REPNE REPNZ REPZ int3
loop LOOPE LOOPNE LOOPNZ LOOPZ'''.lower().split() # wikipedia

ISA_8087 = '''F2XM1 FABS FADD FADDP FBLD FBSTP FCHS FCLEX FCOM FCOMP FCOMPP
FDECSTP FDISI FDIV FDIVP FDIVR FDIVRP FENI FFREE FIADD FICOM FICOMP FIDIV
FIDIVR FILD FIMUL FINCSTP FINIT FIST FISTP FISUB FISUBR FLD FLD1 FLDCW FLDENV
FLDENVW FLDL2E FLDL2T FLDLG2 FLDLN2 FLDPI FLDZ FMUL FMULP FNCLEX FNDISI FNENI
FNINIT FNOP FNSAVE FNSAVEW FNSTCW FNSTENV FNSTENVW FNSTSW FPATAN FPREM FPTAN
FRNDINT FRSTOR FRSTORW FSAVE FSAVEW FSCALE FSQRT FST FSTCW FSTENV FSTENVW FSTP
FSTSW FSUB FSUBP FSUBR FSUBRP FTST FWAIT FXAM FXCH FXTRACT FYL2X
FYL2XP1'''.lower().split() #wiki

ISA_80186 = '''BOUND ENTER INS LEAVE OUTS POPA PUSHA PUSH IMUL SHL SHR SAL SAR
ROL ROR RCL RCR'''.lower().split() # wiki

ISA_80286 = '''ARPL CLTS LAR LGDT LIDT LLDT LMSW LOADALL LSL LTR SGDT SIDT SLDT
SMSW STR VERR VERW'''.lower().split() # wiki

ISA_80287 = '''FSETPM'''.lower().split()

ISA_80387 = '''FCOS FLDENVD FSAVED FSTENVD FPREM1 FRSTORD FSIN FSINCOS FSTENVD
FUCOM FUCOMP FUCOMPP '''.lower().split()

ISA_80386 = '''BSF BSR BT BTC BTR BTS CDQ CMPSD CWDE IBTS INSD IRETx JECXZ LFS
LGS LSS LODSD LOOPW  LOOPccW LOOPD  LOOPccD MOV to/from CR/DR/TR MOVSD MOVSX
MOVZX OUTSD POPAD POPFD PUSHAD PUSHFD SCASD SETcc SHLD SHRD STOSD XBTS SETA
SETAE  SETB  SETBE  SETC  SETE  SETG  SETGE  SETL  SETLE  SETNA  SETNAE  SETNB
SETNBE  SETNC  SETNE  SETNG  SETNGE  SETNL  SETNLE  SETNO  SETNP  SETNS  SETNZ
SETO  SETP  SETPE  SETPO  SETS  SETZ'''.lower().split() #wiki

ISA_80486 = '''BSWAP CMPXCHG INVD INVLPG WBINVD XADD'''.lower().split() # wiki

ISA_80586 = '''CPUID CMPXCHG8B RDMSR RDTSC WRMSR RSM'''.lower().split()

ISA_80687 = ''' FCMOV FCMOVB FCMOVBE FCMOVE FCMOVNB FCMOVNBE FCMOVNE
FCMOVNU FCMOVU FCOMI FCOMI FCOMIP FUCOMI FUCOMIP'''.lower().split()

ISA_MMX = '''emms movd movq packssdw packsswb packuswb paddb paddd paddsb
paddsw paddusb paddusw paddw pand pandn pcmpeqb pcmpeqd pcmpeqw pcmpgtb pcmpgtd
pcmpgtw pmaddwd pmulhw pmullw por pslld psllq psllw psrad psraw psrld psrlq
psrlw psubb psubd psubsb psubsw psubusb psubusw psubw punpckhbw punpckhdq
punpcklbw punpckldq punpcklwd pxor'''.lower().split() # intrinsics guide

ISA_SSE = ''' MASKMOVQ MOVNTPS MOVNTQ NOP NOP PREFETCHT0 PREFETCHT1 PREFETCHT2
PREFETCHNTA SFENCE addps addss andnps andps cmpps cmpss comiss cvtpi2ps
cvtps2pi cvtsi2ss cvtss2si cvttps2pi cvttss2si divps divss ldmxcsr maskmovq
maxps maxss minps minss movaps movhlps movhps movlhps movlps movmskps movntps
movntq movq movss movups mulps mulss orps pavgb pavgw pextrw pinsrw pmaxsw
pmaxub pminsw pminub pmovmskb pmulhuw prefetchnta prefetcht0 prefetcht1
prefetcht2 psadbw pshufw rcpps rcpss rsqrtps rsqrtss sfence shufps sqrtps
sqrtss stmxcsr subps subss ucomiss unpckhps unpcklps xorps VCMPEQPS  VCMPLTPS
VCMPNEQPS VCMPLEPS VCMPUNORDPS VCMPNLTPS VCMPNLEPS VCMPORDPS VCMPEQ_UQPS
VCMPNGEPS VCMPNGTPS VCMPFALSEPS VCMPNEQ_OQPS VCMPGEPS VCMPGTPS VCMPTRUEPS
VCMPEQ_OSPS VCMPLT_OQPS VCMPLE_OQPS VCMPUNORD_SPS VCMPNEQ_USPS VCMPNLT_UQPS
VCMPNLE_UQPS VCMPORD_SPS VCMPEQ_USPS VCMPNGE_UQPS VCMPNGT_UQPS VCMPFALSE_OSPS
VCMPNEQ_OSPS VCMPGE_OQPS VCMPGT_OQPS '''.lower().split()

ISA_SSE2 = '''CLFLUSH LFENCE MFENCE MOVNTI PAUSE xorpd maskmovdqu maxpd maxsd
mfence minpd minsd movapd movd movdq2q movdqa movdqu movhpd movlpd movmskpd
movntdq movnti movntpd movq movq2dq movsd movupd mulpd mulsd orpd packssdw
packsswb packuswb paddb paddd paddq paddsb paddsw paddusb paddusw paddw pand
pandn pause pavgb pavgw pcmpeqb pcmpeqd pcmpeqw pcmpgtb pcmpgtd pcmpgtw pextrw
pinsrw pmaddwd pmaxsw pmaxub pminsw pminub pmovmskb pmulhuw pmulhw pmullw
pmuludq por psadbw pshufd pshufhw pshuflw pslld pslldq psllq psllw psrad psraw
psrld psrldq psrlq psrlw psubb psubd psubq psubsb psubsw psubusb psubusw psubw
punpckhbw punpckhdq punpckhqdq punpckhwd punpcklbw punpckldq punpcklqdq
punpcklwd pxor shufpd sqrtpd sqrtsd subpd subsd ucomisd unpckhpd unpcklpd
lfence addpd addsd andnpd andpd clflush cmppd cmpsd comisd cvtdq2pd cvtdq2ps
cvtpd2dq cvtpd2pi cvtpd2ps cvtpi2pd cvtps2dq cvtps2pd cvtsd2si cvtsd2ss
cvtsi2sd cvtss2sd cvttpd2dq cvttpd2pi cvttps2dq cvttsd2si divpd divsd
'''.lower().split()

ISA_EM64T = '''CDQE CQO CMPSQ CMPXCHG16B IRETQ JRCXZ LODSQ MOVSXD POPFQ PUSHFQ
RDTSCP SCASQ STOSQ SWAPGS'''.lower().split() # x86_64

ISA_SSE3 = '''addsubpd addsubps haddpd haddps hsubpd hsubps lddqu movddup
movshdup movsldup '''.lower().split()

ISA_SSSE3 = '''pabsb pabsd pabsw palignr phaddd phaddsw phaddw phsubd phsubsw
phsubw pmaddubsw pmulhrsw pshufb psignb psignd psignw'''.lower().split()

ISA_SSE41 = '''blendpd blendps blendvpd blendvps dppd dpps extractps insertps
movntdqa mpsadbw packusdw pblendvb pblendw pcmpeqq pextrb pextrd pextrq
phminposuw pinsrb pinsrd pinsrq pmaxsb pmaxsd pmaxud pmaxuw pminsb pminsd
pminud pminuw pmovsxbd pmovsxbq pmovsxbw pmovsxdq pmovsxwd pmovsxwq pmovzxbd
pmovzxbq pmovzxbw pmovzxdq pmovzxwd pmovzxwq pmuldq pmulld ptest roundpd
roundps roundsd roundss '''.lower().split()

ISA_SSE42 = '''crc32 pcmpestri pcmpestrm pcmpgtq pcmpistri pcmpistrm
'''.lower().split()

ISA_AVX = '''vorpd vorps vperm2f128 vpermilpd vpermilps vptest vpxor vrcpps
vroundpd vroundps vrsqrtps vshufpd vshufps vsqrtpd vsqrtps vsubpd vsubps
vtestpd vtestps vunpckhpd vunpckhps vunpcklpd vunpcklps vxorpd vxorps vzeroall
vzeroupper movss vaddpd vaddps vaddsubpd vaddsubps vandnpd vandnps vandpd
vandps vblendpd vblendps vblendvpd vblendvps vbroadcastf128 vbroadcastsd
vbroadcastss vcmppd vcmpps vcmpsd vcmpss vcvtdq2pd vcvtdq2ps vcvtpd2dq
vcvtpd2ps vcvtps2dq vcvtps2pd vcvttpd2dq vcvttps2dq vdivpd vdivps vdpps
vextractf128 vhaddpd vhaddps vhsubpd vhsubps vinsertf128 vlddqu vmaskmovpd
vmaskmovps vmaxpd vmaxps vminpd vminps vmovapd vmovaps vmovddup vmovdqa vmovdqu
vmovmskpd vmovmskps vmovntdq vmovntpd vmovntps vmovshdup vmovsldup vmovupd
vmovups vmulpd vmulps vpcmpistri
VPINSRB VPINSRD VPINSRQ
'''.lower().split()

ISA_AVX2 = '''movd movddup movsd vbroadcasti128 vbroadcastsd vbroadcastss
vextracti128 vgatherdpd vgatherdps vgatherqpd vgatherqps vinserti128 vmovntdqa
vmpsadbw vpabsb vpabsd vpabsw vpackssdw vpacksswb vpackusdw vpackuswb vpaddb
vpaddd vpaddq vpaddsb vpaddsw vpaddusb vpaddusw vpaddw vpalignr vpand vpandn
vpavgb vpavgw vpblendd vpblendvb vpblendw vpbroadcastb vpbroadcastd
vpbroadcastq vpbroadcastw vpcmpeqb vpcmpeqd vpcmpeqq vpcmpeqw vpcmpgtb vpcmpgtd
vpcmpgtq vpcmpgtw vperm2i128 vpermd vpermpd vpermps vpermq vpgatherdd
vpgatherdq vpgatherqd vpgatherqq vphaddd vphaddsw vphaddw vphsubd vphsubsw
vphsubw vpmaddubsw vpmaddwd vpmaskmovd vpmaskmovq vpmaxsb vpmaxsd vpmaxsw
vpmaxub vpmaxud vpmaxuw vpminsb vpminsd vpminsw vpminub vpminud vpminuw
vpmovmskb vpmovsxbd vpmovsxbq vpmovsxbw vpmovsxdq vpmovsxwd vpmovsxwq vpmovzxbd
vpmovzxbq vpmovzxbw vpmovzxdq vpmovzxwd vpmovzxwq vpmuldq vpmulhrsw vpmulhuw
vpmulhw vpmulld vpmullw vpmuludq vpor vpsadbw vpshufb vpshufd vpshufhw vpshuflw
vpsignb vpsignd vpsignw vpslld vpslldq vpsllq vpsllvd vpsllvq vpsllw vpsrad
vpsravd vpsraw vpsrld vpsrldq vpsrlq vpsrlvd vpsrlvq vpsrlw vpsubb vpsubd
vpsubq vpsubsb vpsubsw vpsubusb vpsubusw vpsubw vpunpckhbw vpunpckhdq
vpunpckhqdq vpunpckhwd vpunpcklbw vpunpckldq vpunpcklqdq vpunpcklwd vpxor
vpextrb vpextrd vpextrq
'''.lower().split()

ISA_AVX512F = '''vaddpd vaddps vaddsd vaddss valignd valignq vblendmpd
vblendmps vbroadcastf32x4 vbroadcastf64x4 vbroadcasti32x4 vbroadcasti64x4
vbroadcastsd vbroadcastss vcmppd vcmpps vcmpsd vcmpss vcomisd vcomiss
vcompresspd vcompressps vcvtdq2pd vcvtdq2ps vcvtne2ps2bf16 vcvtneps2bf16
vcvtpd2dq vcvtpd2ps vcvtpd2udq vcvtph2ps vcvtps2dq vcvtps2pd vcvtps2ph
vcvtps2udq vcvtsd2si vcvtsd2ss vcvtsd2usi vcvtsi2sd vcvtsi2ss vcvtss2sd
vcvtss2si vcvtss2usi vcvttpd2dq vcvttpd2udq vcvttps2dq vcvttps2udq vcvttsd2si
vcvttsd2usi vcvttss2si vcvttss2usi vcvtudq2pd vcvtudq2ps vcvtusi2sd vcvtusi2ss
vdivpd vdivps vdivsd vdivss vdpbf16ps vexpandpd vexpandps vextractf32x4
vextractf64x4 vextracti32x4 vextracti64x4 vfixupimmpd vfixupimmps vfixupimmsd
vfixupimmss vfmadd132pd vfmadd132ps vfmadd132sd vfmadd132ss vfmadd213pd
vfmadd213ps vfmadd213sd vfmadd213ss vfmadd231pd vfmadd231ps vfmadd231sd
vfmadd231ss vfmaddsub132pd vfmaddsub132ps vfmaddsub213pd vfmaddsub213ps
vfmaddsub231pd vfmaddsub231ps vfmsub132pd vfmsub132ps vfmsub132sd vfmsub132ss
vfmsub213pd vfmsub213ps vfmsub213sd vfmsub213ss vfmsub231pd vfmsub231ps
vfmsub231sd vfmsub231ss vfmsubadd132pd vfmsubadd132ps vfmsubadd213pd
vfmsubadd213ps vfmsubadd231pd vfmsubadd231ps vfnmadd132pd vfnmadd132ps
vfnmadd132sd vfnmadd132ss vfnmadd213pd vfnmadd213ps vfnmadd213sd vfnmadd213ss
vfnmadd231pd vfnmadd231ps vfnmadd231sd vfnmadd231ss vfnmsub132pd vfnmsub132ps
vfnmsub132sd vfnmsub132ss vfnmsub213pd vfnmsub213ps vfnmsub213sd vfnmsub213ss
vfnmsub231pd vfnmsub231ps vfnmsub231sd vfnmsub231ss vgatherdpd vgatherdps
vgatherqpd vgatherqps vgetexppd vgetexpps vgetexpsd vgetexpss vgetmantpd
vgetmantps vgetmantsd vgetmantss vinsertf32x4 vinsertf64x4 vinserti32x4
vinserti64x4 vmaxpd vmaxps vmaxsd vmaxss vminpd vminps vminsd vminss vmovapd
vmovaps vmovddup vmovdqa32 vmovdqa64 vmovdqu32 vmovdqu64 vmovntdq vmovntdqa
vmovntpd vmovntps vmovsd vmovshdup vmovsldup vmovss vmovupd vmovups vmulpd
vmulps vmulsd vmulss vp2intersectd vp2intersectq vpabsd vpabsq vpaddd vpaddq
vpandd vpandnd vpandnq vpandq vpblendmd vpblendmq vpbroadcastd vpbroadcastq
vpcmpd vpcmpeqd vpcmpeqq vpcmpgtd vpcmpgtq vpcmpq vpcmpud vpcmpuq vpcompressd
vpcompressq vpermd vpermi2d vpermi2pd vpermi2ps vpermi2q vpermilpd vpermilps
vpermpd vpermps vpermq vpermt2d vpermt2pd vpermt2ps vpermt2q vpexpandd
vpexpandq vpgatherdd vpgatherdq vpgatherqd vpgatherqq vpmaxsd vpmaxsq vpmaxud
vpmaxuq vpminsd vpminsq vpminud vpminuq vpmovdb vpmovdw vpmovqb vpmovqd vpmovqw
vpmovsdb vpmovsdw vpmovsqb vpmovsqd vpmovsqw vpmovsxbd vpmovsxbq vpmovsxdq
vpmovsxwd vpmovsxwq vpmovusdb vpmovusdw vpmovusqb vpmovusqd vpmovusqw vpmovzxbd
vpmovzxbq vpmovzxdq vpmovzxwd vpmovzxwq vpmuldq vpmulld vpmuludq vpord vporq
vprold vprolq vprolvd vprolvq vprord vprorq vprorvd vprorvq vpscatterdd
vpscatterdq vpscatterqd vpscatterqq vpshufd vpslld vpsllq vpsllvd vpsllvq
vpsrad vpsraq vpsravd vpsravq vpsrld vpsrlq vpsrlvd vpsrlvq vpsubd vpsubq
vpternlogd vpternlogq vptestmd vptestmq vptestnmd vptestnmq vpunpckhdq
vpunpckhqdq vpunpckldq vpunpcklqdq vpxord vpxorq vrcp14pd vrcp14ps vrcp14sd
vrcp14ss vrndscalepd vrndscaleps vrndscalesd vrndscaless vrsqrt14pd vrsqrt14ps
vrsqrt14sd vrsqrt14ss vscalefpd vscalefps vscalefsd vscalefss vscatterdpd
vscatterdps vscatterqpd vscatterqps vshuff32x4 vshuff64x2 vshufi32x4 vshufi64x2
vshufpd vshufps vsqrtpd vsqrtps vsqrtsd vsqrtss vsubpd vsubps vsubsd vsubss
vucomiss vunpckhpd vunpckhps vunpcklpd vunpcklps'''.lower().split()

ISA_AVX512CD = '''vpbroadcastmb2q vpbroadcastmw2d vpconflictd vpconflictq
vplzcntd vplzcntq'''.lower().split()

ISA_AVX512BW = '''kaddd kaddq kandd kandnd kandnq kandq kmovd kmovq knotd knotq
kord korq kortestd kortestq kshiftld kshiftlq kshiftrd kshiftrq ktestd ktestq
kunpckdq kunpckwd kxnord kxnorq kxord kxorq vpabsb vpabsw vpackssdw vpacksswb
vpackusdw vpackuswb vpaddb vpaddsb vpaddsw vpaddusb vpaddusw vpaddw vpalignr
vpavgb vpavgw vpblendmb vpblendmw vpbroadcastb vpbroadcastw vpcmpb vpcmpub
vpcmpuw vpcmpw vpermi2w vpermt2w vpermw vpmaddubsw vpmaddwd vpmaxsb vpmaxsw
vpmaxub vpmaxuw vpminsb vpminsw vpminub vpminuw vpmovb2m vpmovm2b vpmovm2w
vpmovswb vpmovsxbw vpmovuswb vpmovw2m vpmovwb vpmovzxbw vpmulhrsw vpmulhuw
vpmulhw vpmullw vpsadbw vpshufb vpshufhw vpshuflw vpslldq vpsllvw vpsllw
vpsravw vpsraw vpsrldq vpsrlvw vpsrlw vpsubb vpsubsb vpsubsw vpsubusb vpsubusw
vpsubw vptestmb vptestmw vptestnmb vptestnmw vpunpckhbw vpunpckhwd vpunpcklbw
vpunpcklwd'''.lower().split()

ISA_OTHER = '''adc adcx adox aesdec aesdeclast aesenc aesenclast aesimc
aeskeygenassist andn bextr blsi blsmsk blsr bndcl bndcn bndcu bndmk bndstx bsf
bsr bswap bt btc btr bts bzhi cldemote clflushopt clrssbsy clwb encls enclu
enclv fxrstor fxrstor64 fxsave fxsave64 incsspd incsspq invpcid lzcnt monitor
movbe movdir64b movdiri mulx mwait pclmulqdq pconfig pdep pext popcnt
prefetchwt1 ptwrite rdfsbase rdgsbase rdpid rdpmc rdrand rdseed rdsspd rdsspq
rdtsc rdtscp rol ror rstorssp saveprevssp sbb setssbsy sha1msg1 sha1msg2
sha1nexte sha1rnds4 sha256msg1 sha256msg2 sha256rnds2 tpause tzcnt umonitor
umwait vaesdec vaesdeclast vaesenc vaesenclast vcvtph2ps vcvtps2ph
vgf2p8affineinvqb vgf2p8affineqb vgf2p8mulb vpclmulqdq wbinvd wbnoinvd wrfsbase
wrgsbase wrssd wrssq wrussd wrussq xabort xbegin xend xgetbv xrstor xrstor64
xrstors xrstors64 xsave xsave64 xsavec xsavec64 xsaveopt xsaveopt64 xsaves
xsetbv xtest'''.lower().split() # intel intrinsic guide

ISA_UNK = '''movabs data16 RDPMC'''.lower().split() # misc or unknown

class ISAStat(object):
    ISAs_amd64 = ('8086', '8087', '80186', '80286', '80287', '80386', '80387',
            '80486', '80586', '80687', 'MMX', 'SSE', 'SSE2', 'EM64T',
            'SSE3', 'SSSE3', 'SSE41', 'SSE42', 'AVX', 'AVX2', 'AVX512F',
            'AVX512CD', 'AVX512BW',
            'OTHER', 'UNK')
    @staticmethod
    def genStat_amd64() -> Dict:
        return {x: 0 for x in ISAStat.ISAs_amd64}
    @staticmethod
    def baselineViolation(isa: str) -> bool:
        if isa in ('SSE3', 'SSSE3', 'SSE41', 'SSE42', 'AVX', 'AVX2', 'AVX512F',
                'AVX512CD', 'AVX512BW'):
            return True
        else:
            return False
    @staticmethod
    def pretty(d: Dict) -> None:
        msg = ''
        for (k,v) in d.items():
            if v > 0 and not ISAStat.baselineViolation(k):
                msg += f'\x1b[1;32m{k}\x1b[;m[\x1b[36;1m{v}\x1b[;m] '
            elif v > 0 and ISAStat.baselineViolation(k):
                msg += f'\x1b[1;31m{k}\x1b[;m[\x1b[36;1m{v}\x1b[;m] '
            else:
                continue
                msg += f'{k}[\x1b[38;1m{v}\x1b[;m] '
        return msg
    @staticmethod
    def instructionCategory(instruction: str, reg: str) -> str:
        for isa in ISAStat.ISAs_amd64:
            if instruction.lower() in globals()[f'ISA_{isa}']:
                return isa
        workaround = {
            'vmovd': 'SSE2', 'vmovq': 'SSE2', # https://www.felixcloutier.com/x86/movd:movq
            'vpextrw': 'AVX', # https://www.felixcloutier.com/x86/pextrw
            'VMOVHPS'.lower(): 'AVX', # https://www.felixcloutier.com/x86/movhps
            'vucomisd': 'AVX', # https://www.felixcloutier.com/x86/ucomisd
            }
        if instruction.lower() in workaround.keys():
            return workaround[instruction]
        if instruction.lower() in ('ds', 'cs', 'ss', 'es', 'fs', 'gs', 'addr32', 'rex.XB'.lower(), 'rex.RXB'.lower()):
            return 'UNK' # maybe some special syntax of the assembler
        if 'rex' in instruction:
            return 'UNK'
        if instruction.lower() == '(bad)':
            return 'UNK'
        if '(' in instruction:
            return 'UNK'
        raise ValueError(f'what is instruction "{instruction}" "{reg}"?')
    @staticmethod
    def fileStat(path: str, *, verbose: bool=False) -> Dict:
        stat = ISAStat.genStat_amd64()
        tp = magic.from_file(path, mime=True)
        if tp in ('application/x-pie-executable', 'application/x-sharedlib', 'application/x-executable'):
            if verbose: print('(',end=''); sys.stdout.flush()
            objdump = os.popen(f'objdump -Mintel -wd {path}').readlines()
            if verbose: print('\x1b[1;33mD\x1b[;m',end=''); sys.stdout.flush()
            for line in (x.strip() for x in objdump):
                # FIXME: should categorize by instruction+register
                if re.match(r'^[a-e0-9]+:\t[a-e0-9 ]+\t(\S+)\s?.*', line):
                    inst, reg = re.match(r'^[a-e0-9]+:\t[a-e0-9 ]+\t(\S+)\s?(.+)?', line).groups()
                    cate = ISAStat.instructionCategory(inst, reg)
                    stat[cate] += 1
                else:
                    pass
                #print(line)
                #print(ISAStat.pretty(stat))
            if verbose: print('\x1b[1;33mA\x1b[;m)',end=' '); sys.stdout.flush()
        else:
            print(f'(\x1b[1;33mreject\x1b[;m) "{tp}"', end='')
        return stat
    @staticmethod
    def dirStat(path: str, *, verbose: bool=False) -> Dict:
        files = glob.glob(os.path.join(path, '**'), recursive=True)
        files = [x for x in files if os.path.isfile(x)] # filter
        print(f'* Found {len(files)} files to analyze.')
        dstat = {}
        for (i, f) in enumerate(sorted(files), 1):
            if verbose:
                print(f'[{i}/{len(files)}] \x1b[0;35m{f}\x1b[;m ', end='')
            stat = ISAStat.fileStat(f, verbose=verbose)
            print(ISAStat.pretty(stat))
            dstat[f] = stat
        return dstat

if __name__ == '__main__':

    ag = argparse.ArgumentParser()
    ag.add_argument('-f', '--file',   type=str, default=None)
    ag.add_argument('-d', '--dir',    type=str, default=None)
    ag.add_argument('-o', '--output', type=str, default='ISA-STAT.json')
    ag.add_argument('-v', '--verbose',action='store_true')
    ag.add_argument('--db', type=str, default='nasmdocb.html') # database
    ag = ag.parse_args(sys.argv[1:])
    ag.verbose = True # FIXME: fix output formatting when verbose=False

    if all(x is None for x in (ag.file, ag.dir)):
        raise Exception("either -f or -d")
    if all(x is not None for x in (ag.file, ag.dir)):
        raise Exception("either -f or -d")

    if ag.file is not None:
        print(f'\x1b[0;35m{ag.file}\x1b[;m', end=' : ')
        sys.stdout.flush()
        stat = ISAStat.fileStat(ag.file, verbose=ag.verbose)
        print(ISAStat.pretty(stat))
        with open(ag.output, 'wt') as f:
            f.write(json.dumps(stat, indent=2))
    elif ag.dir is not None:
        dstat = ISAStat.dirStat(ag.dir, verbose=ag.verbose)
        with open(ag.output, 'wt') as f:
            f.write(json.dumps(dstat, indent=2))
