1. download the most popular (e.g. vote > 10000) sources

 $ mkdir srcs/; cd srcs
 $ bash ../download-source.bash
 $ cd ..

2. prepare baseline directory and skylake directory

 $ mkdir baseline skylake
 $ rsync -av srcs/ baseline/
 $ rsync -av srcs/ skylake/

3. build baseline packages

 $ cd baseline
 $ bash ../normal-build.sh

4. build SIMDebian's dpkg variant

 $ cd dpkg; sbuild -j16

5. build skylake packages

 $ cd skylake
 $ bash ../simdebian-build.sh

6. extract all the resulting debs

7. scan and analyse ELF binaries

8. compare

