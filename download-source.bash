#!/bin/bash
set -e
POPCON="https://popcon.debian.org/by_vote"

if [[ ! -r popcon ]]; then
	wget -c $POPCON -O popcon
fi

THRESH=10000
AWKS="(\$1==1){start=1;}(\$4>$THRESH){if (start){print \$2}}(\$4<$THRESH){start=0}"
srcs=( $(cat popcon | mawk "$AWKS") )
counter=1
#for src in ${srcs[@]}; do
#	echo -e "\x1b[1;31m" "(" $((counter++)) "/" ${#srcs[@]} ")" $src "\x1b[;m"
#	apt source --download-only $src || true
#done
xe -j0 -a apt source -qq --download-only -- ${srcs[@]}
